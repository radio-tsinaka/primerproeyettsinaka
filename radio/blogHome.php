   <!--// Extracto de noticias //-->
    <section class="blog-tsinaka home">  

      <!-- link -->
      <a class="btn-link-all" href="https://www.radiotsinaka.org/yonkuik/noticias/">ver todo <i class="fas fa-chevron-right"></i> </a>

      <!-- titulo de sección -->
      <article class="blog-tsinaka_item">
         <div class="content-text title-section">
           <h2 class="title -seccion">Ejekatanauatilmej</h2>
           <h2 class="title -seccion">Noticiero</h2>
         </div>
      </article>

      <?php query_posts('showposts=3&post_type=Noticias');
        while (have_posts()) : the_post(); ?>
      
      <article class="blog-tsinaka_item bg-white">
          
          <!--// contenedor de extracto //-->
          <div class="content-news">

           <!-- media  imagen & audio  -->
            <?php if( have_rows('mediaPost') ): ?>
		      	<?php while( have_rows('mediaPost') ): the_row();
                $img = get_sub_field('img');
                 $positionx = get_sub_field('positionx');
                $positiony = get_sub_field('positiony');
            ?> 
              <figure 
              class="content-news-img"               
              style=" 
                background-image:url(<?php echo $img; ?>);
                background-position-x:<?php echo $positionx; ?>;
                background-position-y:<?php echo $positiony; ?>;
              "
              >
              
                <?php if( have_rows('audios') ): ?>
                <?php while( have_rows('audios') ): the_row();
                  $audio = get_sub_field('audio');
                ?>
                  <audio controls class="in-extract iru-tiny-player" data-title="Audio">
                    <source src="<?php echo $audio; ?>" type="audio/mpeg">
                  </audio>
                <?php endwhile; else: ?>
                <?php endif; ?> 
              </figure>
            <?php endwhile; else: ?>
            <?php endif; ?> 

            <!-- Extracto de la nota  -->
            <div class="content-text -news">

            <ul class="menu share-blog">
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
              </ul>

              <!-- Fecha de publicación-->
              <small class="date -news"><?php the_date('d/m/y'); ?></small>
              
              <!-- título de nota  -->
              <h2 onclick="location.href='<?php the_permalink() ?>';" class="title -news"><?php the_title(); ?></h2>
				
              <p>
                <?php echo excerpt(22) ?>
              </p>
		
               <!-- botón texto -->
                <a class="btn-text" href="<?php the_permalink() ?>">Leer nota</a>
  
            
            </div>
          </div>
          <!--// fin contenedor de extracto //-->

          <!-- botón de nota  -->
        

      </article>
      
      <?php endwhile; ?>
    </section>
 