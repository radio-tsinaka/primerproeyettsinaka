<?php get_header(); ?>


<section class=" eyecatcher eyecatcher-in" >
    <div class="content-text">
        <span class="subtitle-category">Producciones multimedia</span>
        <h2 class="title_int">
        Videos</h2>
    </div>
</section>

<section class="in-blog">

<div class="grid-x grid-margin-x">
<?php

    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    $args = array(
    'post_type' => 'Videos',
    'posts_per_page' => 3,
    'date' => 'rand',
    'post_status' => 'publish',
    'paged' => $paged
    );

    $themes = new WP_Query( $args );
?>
   
   <?php while ( $themes->have_posts() ) : $themes->the_post(); ?>
        

        <div class="cell  medium-8 medium-offset-2">

            <div class="blog-post">
                    <div class="content-video -in">
                    <?php if( have_rows('video') ): ?>
                    <?php while( have_rows('video') ): the_row();
                        $videos = get_sub_field('videos');
                    ?>
                       
                            <iframe src="<?php echo $videos; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      
                    <?php endwhile; else: ?>
                    <?php endif; ?> 
                        
                    </div>
                    <div class="content-text -in">
                      <!-- <ul class="menu share-blog -in">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                      </ul> -->

                        <!-- Fecha de publicación-->
                        <small class="date -news"><?php the_date('F j, Y'); ?></small>

                         <!-- título de nota  -->
                        <h2 onclick="location.href='<?php the_permalink() ?>';" class="title -news"><?php the_title(); ?></h2>
						
						<p><?php echo the_excerpt(20) ?>				
						</p>
						<a class="btn-text" href="<?php the_permalink() ?>">Ver más </a>

                    </div>
    
    
                    <br>
                  </div>

                  <br>
                  <hr>
                  <br>
                  <br>
     
        </div>
    
        <?php endwhile; wp_reset_postdata(); ?>


    <div class="medium-8 medium-offset-2">
        <?php
            /**
            * paginacion para custom post types
            */
            $big = 9999999;

            $args = array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var( 'paged' )),//toma el valor 1 de la consulta 'paged'
            'show_all' => false,
            'end_size' => 1,
            'mid_size' => 2,
            'prev_next' => true,
            'prev_text' => __('« Anterior', 'atr-opt'),
            'next_text' => __('Siguiente »', 'atr-opt'),
            'type' => 'plain',
            'add_args' => false,
            //$themes es la variable que almacena el new WP_Query();
            'total' => $themes->max_num_pages
            );
        ?>
        
        <div class='paginate-links pagination'>
        <?php echo paginate_links($args); ?>
        </div>
    </div>

</div>

</section>

<?php wp_reset_query()?>

<!--// Banner App //-->
<?php include('calltoaction.php');  wp_reset_query()?>

<?php get_footer(); ?>


