<?php get_header(); ?>


<section class="in-blog">

    <div class="grid-x grid-margin-x">
        <div class="cell  medium-8 medium-offset-2">
            <div class="blog-post">

                  <div class="content-text -in -search">
                        
   


                      <?php
                        $s=get_search_query();
                        $args = array(
                                        's' =>$s
                                    );
                            // The Query
                        $the_query = new WP_Query( $args );
                        if ( $the_query->have_posts() ) {
                                _e("<h2 style='font-weight:bold;color:#000'>Resultados de búqueda: ".get_query_var('s')."</h2>");
                                while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                        ?>
                                        <br>
                                        <!-- Fecha de publicación-->
                                        <small class="date -news"><?php the_date('F j, Y'); ?></small>

                                        <!-- título de nota  -->
                                        <h2 onclick="location.href='<?php the_permalink() ?>';" class="title -news"><?php the_title(); ?></h2>

                                        <p><?php echo the_excerpt(20) ?></p>
                                        <a class="btn-text" href="<?php the_permalink() ?>">Ver más </a>
                                        <br>
                                        <br>
                                        <hr>
                                        <?php
                                }
                            }else{
                        ?>
                                <h2 style='font-weight:bold;color:#000'>Elemento no encontrado</h2>
                                <div class="alert alert-info">
                                <p>Lo sentimos, no enconreamos ninguna coincidencia de busqueda</p>
                                </div>
                        <?php } ?>

                  </div>
        
            </div>     
        </div>
    
  </div>
</section>

<?php wp_reset_query()?>

<!--// Banner App //-->
<?php include('calltoaction.php');  wp_reset_query()?>

<?php get_footer(); ?>


