    <!--// Carrusel principal "eyecatcher" //-->
    <section class="eyecatcher">
      <div class="owl-carousel owl-carousel-eye">

      <?php if( have_rows('carussel') ): ?>
			<?php while( have_rows('carussel') ): the_row();
        $title= get_sub_field('title');
		   $subtitle= get_sub_field('subtitle');
        $descript = get_sub_field('descript');
        $picolor = get_sub_field('picolor');
		  ?> 
        <div class="item"
         style="background-color:<?php echo $picolor; ?>;"
        >

          <!--// texto de entrada //-->
          <article class="content-elements">
            <div class="content-text c-white">
              
              <!-- título -->
			<small><?php echo $subtitle; ?></small>
              <h1 class="title"><?php echo $title; ?></h1>
              <!-- descriptivo -->
              <?php echo $descript; ?>

                <!-- botón texto -->
                <?php if( have_rows('btn') ): ?>
                <?php while( have_rows('btn') ): the_row();
                    $text = get_sub_field('text');
                    $link = get_sub_field('link');
                ?> 
                <a href="<?php echo $link; ?>"><?php echo $text; ?> </a>
                <?php endwhile; else: ?>
                <?php endif; ?> 
              
            </div>
          </article>

          <!--// imagen de entrada //-->
          <article class="content-elements"> 

              <?php if( have_rows('positionimage') ): ?>
              <?php while( have_rows('positionimage') ): the_row();
                     $positionx = get_sub_field('positionx');
                    $positiony = get_sub_field('positiony');
                    $img = get_sub_field('img');
              ?>


          
            <figure 
              class="figure-eyecatcher"
              style=" 
                background-image:url(<?php echo $img; ?>);
                background-position-x:<?php echo $positionx; ?>;
                background-position-y:<?php echo $positiony; ?>;
              "
            ></figure>

              <?php endwhile; else: ?>
             <?php endif; ?> 


          </article>

        </div>

        <?php endwhile; else: ?>
		    <?php endif; ?> 

      </div>
    </section>
