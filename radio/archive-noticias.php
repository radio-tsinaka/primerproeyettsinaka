<?php get_header(); ?>


<section class=" eyecatcher eyecatcher-in" >
    <div class="content-text">
        <span class="subtitle-category"> NOTICIERO</span>
        <h2 class="title_int">
        Ejekat Tanauatilme</h2>
    </div>
</section>

<section class="in-blog">

<div class="grid-x grid-margin-x">
<?php

    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    $args = array(
    'post_type' => 'noticias',
    'posts_per_page' => 6,
    'date' => 'rand',
    'post_status' => 'publish',
    'paged' => $paged
    );

    $themes = new WP_Query( $args );
?>
   
   <?php while ( $themes->have_posts() ) : $themes->the_post(); ?>
        
        <div class="medium-6 large-4 cell">

            <div class="grid-x grid-padding-x grid-margin-x">

                <!-- Imagen  -->
                <!-- media  imagen & audio  -->
                <?php if( have_rows('mediaPost') ): ?>
                    <?php while( have_rows('mediaPost') ): the_row();
                    $img = get_sub_field('img');
                ?> 
                    <figure class="content-news-img-in" data-interchange="[<?php echo $img; ?>, small]">
            
                    <?php if( have_rows('audios') ): ?>
                        <?php while( have_rows('audios') ): the_row();
                            $audio = get_sub_field('audio');
                            ?>
                            <audio controls class="iru-tiny-player" data-title="Audio Noticiero">
                                <source src="<?php echo $audio; ?>" type="audio/mpeg">
                            </audio>
                        <?php endwhile; else: ?>
                        <?php endif; ?> 
                    </figure>
                <?php endwhile; else: ?>
                <?php endif; ?> 

                <!-- Contenedor de texto -->
                <div class=" large-12 cell">
                    <div class="content-text -in">

                        <!-- Fecha de publicación-->
                        <small class="date -news"><?php the_date('F j, Y'); ?></small>

                        <!-- Título -->
                        <h3 onclick="location.href='<?php the_permalink() ?>';" class="title"><?php the_title(); ?></h3>
                        
                      <!-- botón texto -->
                        <a class="btn-text" href="<?php the_permalink() ?>">Leer nota</i> </a>
                    </div>
                </div>

            </div>
            <br>
            <br>  
        </div>
    
        <?php endwhile; wp_reset_postdata(); ?>



    <div class="medium-12 cell">
        <?php
            /**
            * paginacion para custom post types
            */
            $big = 9999999;

            $args = array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var( 'paged' )),//toma el valor 1 de la consulta 'paged'
            'show_all' => false,
            'end_size' => 1,
            'mid_size' => 2,
            'prev_next' => true,
            'prev_text' => __('« Anterior', 'atr-opt'),
            'next_text' => __('Siguiente »', 'atr-opt'),
            'type' => 'plain',
            'add_args' => false,
            //$themes es la variable que almacena el new WP_Query();
            'total' => $themes->max_num_pages
            );
        ?>
        <hr>
        <div class='paginate-links pagination'>
        <?php echo paginate_links($args); ?>
        </div>
    </div>

</div>

</section>

<?php wp_reset_query()?>

<!--// Banner App //-->
<?php include('calltoaction.php');  wp_reset_query()?>

<?php get_footer(); ?>


