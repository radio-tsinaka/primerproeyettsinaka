<?php get_header(); ?>

<!-- variables -->
<?php	
    $title = get_the_title();
    $currentpage = $_SERVER['REQUEST_URI'];
 
    $postid = get_post_type();
    $categoryvariable = get_the_category();    
    $id = get_the_ID();
    $args = array(
    'post_type' => $postid,	
    'category_name' => $categoryvariable[0]->name,
    'showposts' => 3,     
    'order'=> 'ASC',		
    'post__not_in' => array( $id )
    );
    $argssmall = 'post_type=' . $postid . '&showposts=5&order=ASC';    
    ?>

<nav class="breadcrumbs-content" aria-label="You are here:" role="navigation">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
    <?php
    if(function_exists('bcn_display'))
    {
    bcn_display();
    }?>
    </div>
</nav>




<section class="in-blog -news">
 
 <div class="grid-x grid-margin-x" id="content">
<?php  while (have_posts()) : the_post(); ?>

     <div class="medium-8 cell">

         <!-- imagen ¬ audio -->
         <div class="content-news-in">
			 
           <?php if( have_rows('mediaPost') ): ?>
                     <?php while( have_rows('mediaPost') ): the_row();
                   $img = get_sub_field('img');
               ?> 
                 <figure class="content-news-img" data-interchange="[<?php echo $img; ?>, small]"> </figure>

                   <?php if( have_rows('audios') ): ?>
                   <?php while( have_rows('audios') ): the_row();
                     $audio = get_sub_field('audio');
                   ?>
                    <audio controls class="in-extract iru-tiny-player -playerin" data-title="">
                       <source src="<?php echo $audio; ?>" type="audio/mpeg">
                     </audio> 
                   <?php endwhile; else: ?>
                   <?php endif; ?> 
               <?php endwhile; else: ?>
               <?php endif; ?> 
			
         </div>


        <!-- titulares -->
        <div class="blog-post">
		
          <!-- video -->
         <div class="content-video -in">
            <?php if( have_rows('video') ): ?>
            <?php while( have_rows('video') ): the_row();
                $videos = get_sub_field('videos');
            ?>
                
                    <iframe src="<?php echo $videos; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                
            <?php endwhile; else: ?>
            <?php endif; ?> 
                
            </div>
			
			<!-- programa o serie-->
                <?php if( have_rows('serie') ): ?>
                    <?php while( have_rows('serie') ): the_row();
                    $img = get_sub_field('img');
                ?> 
               
                            
                    <!-- imágen -->
                    <div class=" medium-12 large-5">
                        <figure
                            class="figureblogdetail"
                            style="background-image: url(<?php echo $img; ?>); "
                        >
                        </figure>
                    </div>

                  <?php endwhile; else: ?>
                  <?php endif; ?> 
			
				<?php echo sharethis_inline_buttons(); ?>
                <div class="content-text -in">
                    <small class="date -news"><?php the_date('F j, Y'); ?></small>

                    <h2 class="title -lg"><?php the_title(); ?></h2>
                    <!-- Fecha de publicación-->
                    <div class="callout">
                        <ul class="menu simple">
                            <!-- <li>Autor: <?php the_author(); ?></li> -->
                            <li><?php the_tags(); ?></li>
                        </ul>
                    </div>
				


                <!-- Título de la nota  -->
                <br>
            </div>

        </div>
		


       <hr>

       <!-- contenido de la nota  -->
        <div class="blog-post">
					 
		 	 <?php if( have_rows('serie') ): ?>
                    <?php while( have_rows('serie') ): the_row();
                    $img = get_sub_field('img');
                    $audio = get_sub_field('audio');
                    $title = get_sub_field('title');
                ?> 
			<br>
			  <div class="large-12 cell p-relative">
                                  <audio controls class=" iru-tiny-player" data-title=" Audio <?php the_title(); ?>">
                                    <source src="<?php echo $audio; ?>" type="audio/mpeg">
                                    </audio>
                                </div>
			 
			    <?php endwhile; else: ?>
               <?php endif; ?> <br>
		
            <div class="content-text -in -news -aside"><br>
                <?php the_content(); ?>
            </div>
        </div>
     
     </div>

     <?php endwhile; ?>

     <div class="medium-4 cell relations">
     <h5 class="title bold">Artículos relacionados</h5>
     <br>
      <?php
            query_posts( $args );
              while (have_posts()) : the_post(); ?>				
                        <!-- programa o serie-->
                    <?php if( have_rows('serie') ): ?>
                    <?php while( have_rows('serie') ): the_row();
                    $img = get_sub_field('img');
                    $audio = get_sub_field('audio');
                    $title = get_sub_field('title');
                ?> 
                      <div class="medium-12 cell">
                              
                          <div class="grid-x grid-margin-x p-relative">
                             <!-- imágen -->
                              <div class="cell ">
                                  <figure class="figureblogdetail"  style=" background-image: url(<?php echo $img; ?>);"> </figure>                 
                              </div>

                              <!-- Título del programa o serie  -->
                              <div class="cell">
                                <div class="content-text -in">

                                  <span class="date"><?php the_date('F j, Y'); ?> </span>
                                  <h3 class="title"><?php the_title(); ?></h3>
                                  <div class="callout">
                                      <ul class="menu simple">
                                          <!-- <li>Autor: <?php the_author(); ?></li> -->
                                          <li><?php the_tags(); ?></li>
                                      </ul>
                                  </div>
        
                                  <a class="btn-text" href="<?php the_permalink() ?>">Leer nota</a>
                  
                                </div>
                              </div>

                              <br><br>

                          </div>  
                          <br>
                          <br>

                     </div>
                  <?php endwhile; else: ?>
                  <?php endif; ?> 
              <?php endwhile; ?>
     </div>

   </div>      
</section>
<?php wp_reset_query()?>

<!--// Banner App //-->
<?php include('calltoaction.php');  wp_reset_query()?>

<?php get_footer(); ?>