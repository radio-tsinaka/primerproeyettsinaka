<?php get_header(); ?>


  <section class=" eyecatcher eyecatcher-in" >
      <div class="content-text">
          <span class="subtitle-category"> PROGRAMA RADIOFÓNICO</span>
          <h2 class="title_int">
          Noticias locales</h2>
      </div>
  </section>

    <!--//// inicio de contenido ////-->
    <section class="in">
        
    <!-- descriptivo del programa o serie  -->
      <div class="grid-x grid-margin-x ">
        <div class="large-10 large-offset-1 cell">
          <div class="content-text text-center">
              <p class="descript"><strong> Lorem, ipsum dolor sit amet</strong> consectetur adipisicing elit. Error harum similique pariatur rem nam maiores tempora, nostrum at eum inventore quas dolorem nulla impedit temporibus aut? Officiis excepturi alias obcaecati!
              </p>
              <br>
          </div>
        </div>
      </div>

            <div class="grid-x grid-margin-x grid-padding-x">
                    <!-- inicio de post  -->
        <?php

        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
        $args = array(
        'post_type' => 'locales',
        'posts_per_page' => 6,
        'date' => 'rand',
        'post_status' => 'publish',
        'paged' => $paged
        );

        $themes = new WP_Query( $args );
        ?>

        <?php while ( $themes->have_posts() ) : $themes->the_post(); ?>
            
                <!-- programa o serie-->
                <?php if( have_rows('serie') ): ?>
                    <?php while( have_rows('serie') ): the_row();
                    $img = get_sub_field('img');
                    $audio = get_sub_field('audio');
                    $title = get_sub_field('title');
                ?> 
                    <div class="medium-6 cell">
                        
                          <div class="grid-x grid-margin-x p-relative">
                            
                              <!-- imágen -->
                                <div class=" medium-12 large-5 cell show-for-medium">
                                  <p><img src="<?php echo $img; ?>" alt="image for article" alt="article preview image"></p>
                                </div>

                              <!-- título del programa o serie  -->
                                <div class=" large-7 cell">
                                  <div class="content-text -in">

                                    <span class="date"><?php the_date('F j, Y'); ?> | <?php the_category(); ?></span>
                                    <h3 class="title"><?php the_title(); ?></h3>
                                    <div class="callout">
                                        <ul class="menu simple">
                                            <!-- <li>Autor: <?php the_author(); ?></li> -->
                                            <li><?php the_tags(); ?></li>
                                        </ul>
                                    </div>
                                    <?php if( have_rows('seriedetail') ): ?>
                                        <?php while( have_rows('seriedetail') ): the_row();
                                        $id = get_sub_field('id');
                                    ?> 
                                      <a type="button" class="btn-text" data-toggle="<?php echo $id; ?>"> Más detalles</a>

                                     
                                     <!-- Detalle del programa o serie  -->
                                      <div class="off-canvas-wrapper">
                      
                                          <div class="off-canvas position-right" id="<?php echo $id; ?>" data-off-canvas>
                                              <div class="content-offcanvas">
                                                  <div class="grid-x grid-margin-x grid-padding-x">
                                                        <div class="medium-8 cell large-offset-2">
                                                            <div class="blog-post">
                                                                <div class="content-text -in -news -aside">
                                                                    <h3 class="title"><?php the_title(); ?></h3>
                                                                    <div class="callout">
                                                                        <ul class="menu simple">
                                    
                                                                            <li><?php the_tags(); ?></li>
                                                                        </ul>
                                                                    </div>
                                                                    <br>
                                                                    <p><?php the_content(); ?></p>
                                                                    <br>
                                                                    <a type="button" class="btn-text"  aria-label="Close menu" type="button" data-close="">
                                                                    <i class="fas fa-long-arrow-alt-left"></i>  
                                                                    Regresar</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                  </div>
                                              </div>
                                          </div>

                                      </div>
                                  <?php endwhile; else: ?>
                                  <?php endif; ?> 


                                  </div>
                                </div>

                              <!-- audio del programa o serie  -->
                                <div class="large-12 cell p-relative">
                                  <audio controls class=" iru-tiny-player" data-title=" Audio ">
                                    <source src="<?php echo $audio; ?>" type="audio/mpeg">
                                    </audio>
                                </div>
                                <br><br>

                          </div>  
                        <br>
                        <br>

                        </div>
                  <?php endwhile; else: ?>
                  <?php endif; ?> 

                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
    <!-- fin de post  -->

    <div class="medium-12 cell">
        <?php
            /**
            * paginacion para custom post types
            */
            $big = 9999999;

            $args = array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var( 'paged' )),//toma el valor 1 de la consulta 'paged'
            'show_all' => false,
            'end_size' => 1,
            'mid_size' => 2,
            'prev_next' => true,
            'prev_text' => __('« Anterior', 'atr-opt'),
            'next_text' => __('Siguiente »', 'atr-opt'),
            'type' => 'plain',
            'add_args' => false,
            //$themes es la variable que almacena el new WP_Query();
            'total' => $themes->max_num_pages
            );
        ?>
        <hr>
        <div class='paginate-links pagination'>
        <?php echo paginate_links($args); ?>
        </div>
    </div>


    </section>

<?php wp_reset_query()?>

<!--// Banner App //-->
<?php include('calltoaction.php');  wp_reset_query()?>

<?php get_footer(); ?>


