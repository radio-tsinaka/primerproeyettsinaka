<!--// Extracto de series radiofónicas //-->
<section class="series-home home" >
       
      <!-- Cabecera de Sección -->
      <article class="series-tsinaka_item">

     
            <!-- título -->
            <div class="content-text title-section">
            <h2 class="title -seccion">Series</h2>
            <h2 class="title -seccion">Radiofónicas</h2>
            </div>

      </article>

      <!-- Carrusel -->
      <div class="owl-carousel owl-carousel-series">

        <?php if( have_rows('series') ): ?>
        <?php while( have_rows('series') ): the_row();
            $img = get_sub_field('img');
		  $kind = get_sub_field('kind');
            $link = get_sub_field('link');
        ?>

            <!-- item carrusel -->
            <div class="item" data-interchange="[<?php echo $img; ?>, small]">
				<small class="category"><?php echo $kind; ?></small>
                <div class="content-text">
                    <!-- botón texto -->
                    <a class="btn-text" href="<?php echo $link; ?>">Ver más</i> </a>
                </div>
            </div>

        <?php endwhile; else: ?>
        <?php endif; ?> 

    </div>
</section>
