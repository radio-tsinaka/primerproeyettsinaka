    <!--// Extracto de programas radiofónicos //-->
    <section class="programas-home home" >
       <!-- Título -->
      <article class="programas-tsinaka_item">
     
        <div class="content-text title-section">
          <h2 class="title -seccion">Programas</h2>
          <h2 class="title -seccion">Radiofónicos</h2>
        </div>
     </article>

        <!--// Contenedor de carrusel //-->
        <div class="owl-carousel owl-carousel-programs">

        <?php if( have_rows('programs') ): ?>
        <?php while( have_rows('programs') ): the_row();
            $img = get_sub_field('img');
            $kind = get_sub_field('kind');
            $title = get_sub_field('title');
            $dates = get_sub_field('dates');
            $link = get_sub_field('link');
        ?> 
        
        <!-- Imagen -->
        <div class="item">
            <figure data-interchange="[<?php echo $img; ?>, small]">
			<small class="category"><?php echo $kind; ?></small>
			</figure>
            <div class="content-text -extract-home">
                  
                <!-- Tipo de producción  -->
   

                <!-- Título  -->
                <h2 class="title"><?php echo $title; ?></h2>

                <!-- Dias de transmisión  -->
                <p class="dates-time"><?php echo $dates; ?></p>

                <a class="btn-text" href="<?php echo $link; ?>">Ver más</i> </a>

            </div>


        </div>

        <?php endwhile; else: ?>
        <?php endif; ?> 

    </div>
    <!--// Fin dek contenedor de carrusel //-->
    </section>
