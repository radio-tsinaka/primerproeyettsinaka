<?php query_posts('post_type=Call_to_action');
    while (have_posts()) : the_post(); ?>
    <?php if( have_rows('images') ): ?>
    <?php while( have_rows('images') ): the_row();
        $imagen_movil = get_sub_field('imagen_movil');
        $imagen_escritorio = get_sub_field('imagen_escritorio');
        $link = get_sub_field('link');
    ?> 
        <section onclick="window.open('<?php echo $link; ?>')" class="banner-app">

            <!-- imagen para version móvil -->
            <?php 
            if( !empty( $imagen_movil ) ): ?>
                <img class="show-for-small-only" src="<?php echo esc_url($imagen_movil['url']); ?>" alt="<?php echo esc_attr($imagen_movil['alt']); ?>" />
            <?php endif; ?>

            <!-- Versión para versión de escritorio  -->
            <?php 
            if( !empty( $imagen_escritorio ) ): ?>
                <img class="show-for-medium" src="<?php echo esc_url($imagen_escritorio['url']); ?>" alt="<?php echo esc_attr($imagen_escritorio['alt']); ?>" />
            <?php endif; ?>

        </section>
        <?php endwhile; else: ?>
        <?php endif; ?> 
    
<?php endwhile; ?>