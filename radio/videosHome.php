    <!-- Extracto sección multimedia inicio-->
    <section class="multimedia-home home">

      <!-- Cabecera de Sección -->
      <article class="series-tsinaka_item">

        <!--btn-->
        <a class="btn-link-all_title" href="https://www.radiotsinaka.org/yonkuik/videos/">VER TODO<i class="fas fa-chevron-right"></i></a>

        <!-- Título de sección-->
        <div class="content-text title-section">
          <h2 class="title -seccion bold">Producciones</h2>
          <h2 class="title -seccion ">Multimedia</h2>
        </div>

      </article>

      <br>

      <!-- Multimedia -->
      <div class="grid-x grid-margin-x">


      <?php query_posts('showposts=1&post_type=Videos');
        while (have_posts()) : the_post(); ?>
        
        <!-- Video -->
        <div class="cell medium-8">
            <?php if( have_rows('video') ): ?>
            <?php while( have_rows('video') ): the_row();
                $videos = get_sub_field('videos');
            ?>
                <div class="content-video">
                    <iframe src="<?php echo $videos; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            <?php endwhile; else: ?>
            <?php endif; ?> 

                <div class="content-news">
                    <div class="content-text -news">
                        
                        <!-- fecha de publicación-->
                        <small class="date -news"><?php the_date(); ?></small>

                        <!-- Título  -->
                        <h2 class="title -news"><?php the_title(); ?></h2>

                        <!-- botón texto -->
                      <a class="btn-text" href="<?php the_permalink() ?>">Leer nota</i> </a>
                    </div>
                </div>
        </div>
        <?php endwhile; wp_reset_query() ?>

        <!-- Video -->
        <div class="cell medium-4 video-home">
        <?php query_posts('showposts=3&post_type=Videos');
        while (have_posts()) : the_post(); ?>

          <div class="video-small">
            <?php if( have_rows('video') ): ?>
            <?php while( have_rows('video') ): the_row();
                $videos = get_sub_field('videos');
            ?>
             <iframe src="<?php echo $videos; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
            </iframe> 
            <?php endwhile; else: ?>
            <?php endif; ?> 
            <div class="content-news">
              <div class="content-text -news">

                    <!-- fecha de publicación-->
                    <small class="date -news"><?php the_date(); ?> | <?php the_category(); ?></small>

                    <!-- Título  -->
                    <h2 class="title -news"><?php the_title(); ?></h2>

                    <!-- botón texto -->
                    <a class="btn-text" href="<?php the_permalink() ?>">Leer nota</i> </a>
                    

              </div>
            </div>
            <br>
          </div> 

          <?php endwhile; wp_reset_query() ?>
        </div>


      </div>
    </section>