<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radio Tsinaka</title>
    <meta name="description" content="">
    <!-- Compressed CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&family=Roboto:wght@500;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php wp_head(); ?>	 
    
    <script type="text/javascript">
        //<![CDATA[
        var win = null;
        function NewWindow(mypage,myname,w,h,scroll){
        LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
        TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
        settings = "height="+h+",width="+w+",top="+TopPosition+",left="+LeftPosition+",resizable=yes,scrollbars="+scroll;
        win = window.open(mypage,myname,settings);
        }
        //]]>
        </script>
</head>

    <?php	
        $title = get_the_title();
        $currentpage = $_SERVER['REQUEST_URI'];
    ?>
<body>
  <!-- Menú versión móbile -->
  <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
    <ul class="vertical menu">
      <li><a class="active" href="/yonkuik/">Inicio</a></li>
      <li><a href="/noticias/">Noticiero</a></li>
      <li><a href="#">Programas</a>
		  <ul class="nested vertical menu">
			   <li><a href="/yonkuik/lavozdelmurcielago/">La voz del murciélago</a></li>
			   <li><a href="/yonkuik/Ximonemachpia/">Ximonemachpia</a></li>
			   <li><a href="/yonkuik/Pipikonemej/">Ojtokalis</a></li>
			   <li><a href="/yonkuik/ximonemachpia/">Ximonemachpia</a></li>
			  <li><a href="/yonkuik/altepetnechikolmej/">Altepetnechikolmej</a></li>
			  <li><a href="/yonkuik/netasmaseual/">Netas Maseual</a></li>
			</ul> 
      </li>
      <li><a href="#">Series Radiofónicas</a>
       <ul class="nested vertical menu">
         <li><a href="/yonkuik/saberdemipueblo/">El Saber de mi Pueblo</a></li>
		   <li><a href="/yonkuik/agendapolitica/">Agenda Política</a></li>
		   <li><a href="/yonkuik/ojtokalis/">Ojtokalis</a></li>
		   <li><a href="/yonkuik/chiupaujkayot/">Chiupaujkayot</a></li>
		   <li><a href="/yonkuik/MaseualPajtij/">MaseualPajtij</a></li>
		   <li><a href="/yonkuik/semillasresistencia/">Semillas en Resistencia</a></li>
        </ul> 
      </li>
      <li><a href="/yonkuik/videos/">Videos</a></li>
      <li><a href="#">Apóyanos</a></li>
    </ul>
    <br>
    <ul class="menu redes">
      <li><a href="#"><i class="fab fa-youtube"></i></a></li>
      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
      <li><a href="#"><i class="fab fa-twitter"></i></a></li>
      <li><a href="#"><i class="fas fa-envelope"></i></a></li>
    </ul>
  </div>

  <!--///// Empieza el código del sitio ////-->
  <div class="off-canvas-content" data-off-canvas-content>

    <!--// Herramientas versión móvil: radio en vivo y btn whatsApp //-->
    <div class="tools-mobile">
        <div class="btn-live">
        
            <div class="btn-live-content">
				
                <a  href="http://giss.tv:8001/tzinaca-radio.mp3" onclick="NewWindow(this.href,'name','320','200','no');return false" class="btn-live_item">
                <i class="fas fa-play-circle"></i>
                    <h3>RADIO TSINAKA <br>EN VIVO</h3>
                </a>

                <div class="btn-live_item">
                  <i class="fab fa-whatsapp"></i>
              </div>

            </div>
        </div>
    </div>

    <!--// Cabercera de sitio //-->
    <header class="header nav-top">
        <div class="grid-x">

          <!-- Botón de menú -->
          <div class="cell small-2">
            <div class="btn-menu-mobile" type="button" data-toggle="offCanvas">
              <i class="fas fa-bars"></i>
            </div>
          </div> 
          
          <!-- Herramientas version móvil -->
          <div class="cell small-10">
            <div class="version-web ">
              
              <!-- idioma
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('idioma') ) : endif; ?>
              -->
            </div>
          </div>

          <!-- logotipo -->
          <div class="cell medium-2">
              <img class="logo-radio-tsinaka" src="<?php bloginfo('template_url') ?>/assets/img/logo-radio-tsinaka.jpg" alt="">
          </div>

          <!-- menú versión escritorio -->
          <div class="menu-all-tools-desktop cell medium-10">
            
            <!-- Herramientas versión escritorio -->
            <ul class="menu align-right tools-desktop">
              <li>
                <!--<div class="version-web-desktop">
                  <small>Idioma:</small>
                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('idioma') ) : endif; ?>
              </div>-->
              </li>
              <li>
                <div class="btn-live">
                  <div class="btn-live-content">
					   
                      <a  href="http://emisoras.com.mx/embed/tsinaka-104-9/?fbclid=IwAR2Ay1uGfR-NquzmVAfPrEgxX9pPhF99UmuflnrH2BeNZtD-XHiw1aJAkOc" onclick="NewWindow(this.href,'name','520','180','yes');return false" class="btn-live_item">
                          <h3>Radio<br>en Vivo</h3>
                        </a>
                      <div class="btn-live_item">
                          <i class="fas fa-play-circle"></i>
                      </div>

                  </div>
              </div>
              </li> 

              <!-- menu de redes sociales y teléfono -->
              <li class="flex-column menu-contact-redes">
                <ul class="menu menu-contact_icons">
                  <li><a target="_blank" href="https://www.youtube.com/channel/UCRq8KnCnBybDJS1shum_sRA"><i class="fab fa-youtube"></i></a></li>
                  <li><a target="_blank" href="https://web.facebook.com/RadioTsinaka104.9FM"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a target="_blank" href="https://twitter.com/radiotsinaka"><i class="fab fa-twitter"></i></a></li>
                  <li><a target="_blank" class="link-wp" href="https://wa.me/522331116368?text=Hola,%20me%20gustaría%20saber%20más%20acerca%20de%20ustedes"><i class="fab fa-whatsapp"></i></a></li>
                </ul>
                  <a class="link-tel" href="#2331116368">TEL: 233 111 63 68</a>
              </li>
            </ul>
            

            <ul class="dropdown menu align-right menu-desktop-lg"  data-dropdown-menu >
              <li><a class="active" href="/yonkuik/">Inicio</a></li>
				  <li><a href="/yonkuik/quienes-somos/">La Radio Tsinaka</a></li>
                  <li><a href="/yonkuik/noticias/">Noticiero</a></li>
              <li>
                <a href="#">Programas</a>
                <ul class="menu"><li><a href="/yonkuik/lavozdelmurcielago/">La voz del murciélago</a></li>
					 <li><a href="/yonkuik/ximonemachpia/">Ximonejmachpiaj</a></li>
					 <li><a href="/yonkuik/pipikonemej/">Pipilkonemej</a></li>
					 <li><a href="/yonkuik/yauitsin/">Yauitsin</a></li>
					 <li><a href="/yonkuik/altepetnechikolmej/">Altepet Nechikolmej</a></li>
					<li><a href="/yonkuik/netasmaseual/">Netas Maseual</a></li>
                </ul>
              </li>
              <li>
                <a href="#">Series Radiofónicas</a>
                <ul class="menu" >
                     <li><a href="/yonkuik/saberdemipueblo/">El Saber de mi Pueblo</a></li>
                      <li><a href="/yonkuik/agendapolitica/">Agenda Política</a></li>
                      <li><a href="/yonkuik/ojtokalis/">Ojtokalis</a></li>
                      <li><a href="/yonkuik/chiupaujkayot/">Chiupaujkayot</a></li>
                      <li><a href="/yonkuik/maseualpajtij/">MaseualPajtij</a></li>
                      <li><a href="/yonkuik/semillasresistencia/">Semillas en Resistencia</a></li>
                </ul>
              </li>
        
              <li><a href="/yonkuik/videos/">Videos</a></li>
             
              <li data-toggle="example-dropdown-bottom-right-dektop"><a><i class="fas fa-search" ></i></a></li>
            </ul>
                <div class="dropdown-pane" data-position="bottom" data-alignment="right" id="example-dropdown-bottom-right-dektop" data-dropdown data-close-on-click="true">
                 <?php get_search_form(); ?>
                </div>
          </div>
         
        </div>
    </header>

  <?php
  if(  $currentpage == '/yonkuik/'
)  {echo '
  <div class="mobil-logo">
  <img class="logo-radio-tsinaka" src="https://gersainestanislao.com/tsinaka/web/assets/img/logo-radio-tsinaka.jpg" alt="">
</div> ';
} else {
      echo '';
    }
?>