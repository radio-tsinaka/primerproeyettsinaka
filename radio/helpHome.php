<?php  
    /*
    Template Name: genérica
    */
?>


<?php get_header(); ?>
   <!-- Extracto de programas de apoyo -->


 <?php if( have_rows('eyein') ): ?>
  <?php while( have_rows('eyein') ): the_row();
    $subtitle = get_sub_field('subtitle');
    $title = get_sub_field('title');
  ?> 
      <section class=" eyecatcher eyecatcher-in" >
          <div class="content-text">
              <span class="subtitle-category"> <?php echo $subtitle; ?></span>
              <h2 class="title_int">
              <?php echo $title; ?></h2>
          </div>
      </section>

  <?php endwhile; else: ?>
  <?php endif; ?> 
  <div class="grid-x grid-margin-x grid-padding-x">

  <?php if( have_rows('textgeneric') ): ?>
  <?php while( have_rows('textgeneric') ): the_row();

  ?> 
      <div class="marketing-site-content-section">
      <?php if( have_rows('contentone') ): ?>
        <?php while( have_rows('contentone') ): the_row();
         $img = get_sub_field('img');
         $title = get_sub_field('title');
         $descript = get_sub_field('descript');

        ?> 
        <div class="marketing-site-content-section-img">
         <img src="<?php echo $img; ?>" alt="" />
        </div>

        <div class="marketing-site-content-section-block">
          <h3 class="marketing-site-content-section-block-header"><?php echo $title; ?></h3>
          <p> <?php echo $descript; ?></p>
        </div>
    <?php endwhile; else: ?>
   <?php endif; ?>

   <?php if( have_rows('contenttwo') ): ?>
        <?php while( have_rows('contenttwo') ): the_row();
         $img = get_sub_field('img');
         $title = get_sub_field('title');
         $descript = get_sub_field('descript');

        ?> 
        <div class="marketing-site-content-section-block small-order-2 medium-order-1">
          <h3 class="marketing-site-content-section-block-header"><?php echo $title; ?></h3>
          <p><?php echo $descript; ?></p>
        </div>
          <div class="marketing-site-content-section-img small-order-1 medium-order-2">
            <img src="<?php echo $img; ?>" alt="" />
          </div>

        <?php endwhile; else: ?>
        <?php endif; ?>
    </div>
    <?php endwhile; else: ?>
  <?php endif; ?>

  
  </div>


   <!--// Banner App //-->
<?php include('calltoaction.php');  wp_reset_query()?>

<?php get_footer(); ?>