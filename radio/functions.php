
<?php

  //1  Añadiedo la hoja de estilos.
  //2  Regsitro de sidebar: plugin de ididoma.
  //3  Formatos que soportan los plugins.
  //4  Plugin para limitar el número de palabras.
  //5  Registro de nuevos post
    //  5.1 // Post de Call to Action
    //  5.2 // Post de Videos
    //  5.3 // Post de Noticias
    //  5.4 // Serie Agenda política
    //  5.5 // Serie El Saber de mi pueblo
    //  5.6 // Serie Ojtokalis
      // 5.6.1 Serie Ojtokalis temporada 1
      // 5.6.2 Serie Ojtokalis temporada 3
    //  5.7 // Serie Semillas en resistencia
    //  5.8 // Serie Chiupaujkayo
    //  5.9 // Serie MaseualPajtij
    //  5.10 // Programa la voz del murciélago
    //  5.11 // Programa Altepetnechikolmej
    //  5.12 // Programa Yauitsin
    //  5.13 // Programa Netas Maseual
    //  5.14 // Programa Pipikonemej
    //  5.15 // Programa Ximonemachpia


function styleMain_style() {
  // ∆∆ Registre Styles & fonts
    wp_enqueue_style( 
      'stylePlugin', 
      get_template_directory_uri() . '/css/style.css' ,
      '',
      null,
      'screen'
      );
  }
  add_action( 'wp_enqueue_scripts', 'styleMain_style' );

  //2- Plugin: idióma----------------------------------------
  register_sidebar(array(
    'name' => 'idioma'
    ));


  //3- Registro support----------------------------------------
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'post-formats', array(
      'aside',
      'image',
      'video',
      'quote',
      'link',
      'gallery',
      'status',
      'audio',
      'chat',
    ) );

  // Limitar cantidad de palabras----------------------------------------
  function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
    }
    else {
    $excerpt = implode(" ",$excerpt);
    }
    
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
  }

  // 5  Registro de nuevos post:----------------------------------------

    // 5.1 Post de Call to Action----------------------------------------
    add_action( 'init', 'Call_to_action' );
    function Call_to_action() {
      $args = array(
      'public' => true,
      'label' => 'Call_to_action',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
      register_post_type( 'Call_to_action', $args );
      }

    // 5.2 Post de Videos----------------------------------------
    add_action( 'init', 'videos' );
    function videos() {
        $args = array(
        'public' => true,
        'label' => 'Videos',
        'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
        'taxonomies' => array('category', 'post_tag'),
        'has_archive' => true
        );
        register_post_type( 'videos', $args );
        }

    // 5.3 Post de Noticias----------------------------------------
    add_action( 'init', 'noticias' );
    function noticias() {
        $args = array(
        'public' => true,
        'label' => 'Noticias',
        'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
        'taxonomies' => array('category', 'post_tag'),
        'has_archive' => true
        );
        register_post_type( 'noticias', $args );
        }

    // 5.4 Serie Agenda política.----------------------------------------
    add_action( 'init', 'agendapolitica' );
      function agendapolitica() {
        $args = array(
        'public' => true,
        'label' => 'Serie Agenda política',
        'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
        'taxonomies' => array('category', 'post_tag'),
        'has_archive' => true
        );
        register_post_type( 'agendapolitica', $args );
        }

    // 5.5 Serie El Saber de mi pueblo----------------------------------------
    add_action( 'init', 'saberdemipueblo' );
    function saberdemipueblo() {
      $args = array(
      'public' => true,
      'label' => 'Serie El saber de mi pueblo',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
    register_post_type( 'saberdemipueblo', $args );
    }

    // 5.6 Serie Ojtokalis----------------------------------------
    add_action( 'init', 'ojtokalis' );
    function ojtokalis() {
      $args = array(
      'public' => true,
      'label' => 'Serie Ojtokalis',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
      register_post_type( 'ojtokalis', $args );
      }

          // 5.6.1 Serie Ojtokalis temporada 1----------------------------------------
          add_action( 'init', 'ojtokalist1' );
          function ojtokalist1() {
            $args = array(
            'public' => true,
            'label' => 'Serie Ojtokalis Temprada 1',
            'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
            'taxonomies' => array('category', 'post_tag'),
            'has_archive' => true
            );
            register_post_type( 'ojtokalist1', $args );
            }

          // 5.6.1 Serie Ojtokalis temporada 3----------------------------------------
          add_action( 'init', 'ojtokalist3' );
          function ojtokalist3() {
            $args = array(
            'public' => true,
            'label' => 'Serie Ojtokalis Temprada 3',
            'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
            'taxonomies' => array('category', 'post_tag'),
            'has_archive' => true
            );
            register_post_type( 'ojtokalist3', $args );
            }

    // 5.7 Serie Semillas en resistencia----------------------------------------
    add_action( 'init', 'semillasresistencia' );
    function semillasresistencia() {
      $args = array(
      'public' => true,
      'label' => 'Serie Semillas en resistencia',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
      register_post_type( 'semillasresistencia', $args );
      }

    // 5.8 Serie Chiupaujkayo----------------------------------------
    add_action( 'init', 'chiupaujkayot' );
    function chiupaujkayot() {
    $args = array(
      'public' => true,
      'label' => 'Serie Chiupaujkayot',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
    register_post_type( 'chiupaujkayot', $args );
    }

    // 5.9 Serie MaseualPajtij----------------------------------------
    add_action( 'init', 'maseualpajtij' );
    function maseualpajtij() {
      $args = array(
      'public' => true,
      'label' => 'Serie MaseualPajtij',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
      register_post_type( 'maseualpajtij', $args );
    }

    // 5.10 Programa la voz del murciélago----------------------------------------
    add_action( 'init', 'lavozdelmurcielago' );
    function lavozdelmurcielago() {
      $args = array(
      'public' => true,
      'label' => 'Programa la voz del murciélago',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
    register_post_type( 'lavozdelmurcielago', $args );
    }

    // 5.11 Programa Altepetnechikolmej----------------------------------------
    add_action( 'init', 'altepetnechikolmej' );
    function altepetnechikolmej() {
      $args = array(
      'public' => true,
      'label' => 'Programa Altepetnechikolmej',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
      register_post_type( 'altepetnechikolmej', $args );
      }

    // 5.12 Programa Yauitsin----------------------------------------
    add_action( 'init', 'yauitsin' );
    function yauitsin() {
      $args = array(
      'public' => true,
      'label' => 'Programa Yauitsin',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
    register_post_type( 'yauitsin', $args );
    }

    // 5.13 Programa Netas Maseual----------------------------------------
    add_action( 'init', 'netasmaseual' );
    function netasmaseual() {
      $args = array(
      'public' => true,
      'label' => 'Netas Maseual',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
    register_post_type( 'netasmaseual', $args );
    }

    // 5.14 Programa Pipikonemej quitar----------------------------------------
    add_action( 'init', 'pipikonemej' );
    function pipikonemej() {
      $args = array(
      'public' => true,
      'label' => 'Programa Pipikonemej',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
    register_post_type( 'pipikonemej', $args );
    }

    // 5.15 Programa Ximonemachpia----------------------------------------
    add_action( 'init', 'ximonemachpia' );
    function ximonemachpia() {
      $args = array(
      'public' => true,
      'label' => 'Programa Ximonemachpia',
      'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
      'taxonomies' => array('category', 'post_tag'),
      'has_archive' => true
      );
      register_post_type( 'ximonemachpia', $args );
      }
?>