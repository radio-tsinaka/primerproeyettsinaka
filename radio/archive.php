<?php get_header(); ?>


<?php wp_reset_query()?>

<section class="in-blog">

<div class="grid-x grid-margin-x">

   <br><br><br><br>
<?php query_posts('');?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


        
        <div class="medium-12 large-12 cell">

            <div class="grid-x grid-padding-x grid-margin-x">

           
                <!-- Contenedor de texto -->
                <div class=" large-12 cell">
                    <div class="content-text -in">

                        <!-- Fecha de publicación-->
                        <small class="date -news"><?php the_date('F j, Y'); ?> | <?php the_category(); ?></small>

                        <!-- Título -->
                        <h3 onclick="location.href='<?php the_permalink() ?>';" class="title"><?php the_title(); ?></h3>
                        
                      <!-- botón texto -->
                        <a class="btn-text" href="<?php the_permalink() ?>">Leer notas</i> </a>
                    </div>
                </div>

            </div>
            <br>
            <br>  
        </div>
    
        <?php endwhile; else: ?>
        <?php endif; ?>


</div>
</section>

<?php wp_reset_query()?>

<!--// Banner App //-->
<?php include('calltoaction.php');  wp_reset_query()?>

<?php get_footer(); ?>


