<?php  
    /*
    Template Name: home
    */
?>

<?php get_header(); ?>

    <!-- Carrusel -->
    <?php include('eyecatcher.php'); ?>

    <!-- Blog -->
    <?php include('blogHome.php'); wp_reset_query() ?>

    <!-- Programas -->
    <?php include('programsHome.php'); ?>

    <!-- Series -->
    <?php include('seriesHome.php'); ?>

    <!-- Videos -->
    <?php include('videosHome.php'); ?>

    <!-- Banner de Apoyo -->
    <?php include('helpHome.php'); ?>


<?php get_footer(); ?>



