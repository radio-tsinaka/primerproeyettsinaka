        <!-- Footer -->
        <footer class="marketing-site-footer">
            <div class="footer-content">
                <div class="grid-x medium-unstack">

                <div class="medium-3 cell">
                    <h3 class="marketing-site-footer-name">Radio Tsinaka</h3>
                    <p>San Miguel Tzinacapan, Cuetzalan <br>
                    del Progreso. <br>
                    Calle: 20 de Noviembre.</p>
                </div>

                <div class="medium-3 cell">

                    <div class="marketing-site-footer-block">
                    <i class="fas fa-phone"></i>
                    <p>233 111 63 68</p>
                    </div>
                    <div class="marketing-site-footer-block">
                    <i class="fas fa-envelope"></i>
                    <p>radiotsinaka@gmail.com</p>
                    </div>
                    <div class="grid-x small-up-6 icons-redes">
                    
                    <div class="cell">
                        <a target="_blank" href="https://www.youtube.com/channel/UCRq8KnCnBybDJS1shum_sRA"><i class="fab fa-youtube"></i></a>
                    </div>
                    <div class="cell">
                        <a target="_blank" href="https://twitter.com/radiotsinaka"><i class="fab fa-facebook-f"></i></a>
                    </div>
                    <div class="cell">
                        <a target="_blank" href="https://twitter.com/radiotsinaka"><i class="fab fa-twitter"></i></a>
                    </div>

                    </div>
                    <br>
                </div>

                <div class="medium-3 cell">

                    <div class="marketing-site-footer-block">
                    <i class="fas fa-download"></i>
                    <p>
                        Escuchanos en tu teléfono
                        descarga nuestra APP</p>
                    </div>

                </div>
                </div>
            </div>
        </footer>
        
        <!-- Derechos de autor -->
        <div class="marketing-site-footer-bottom">
            <div class="content-footer">
                Todos lo derechos reservados, Radio Tsinaka 2021.   |  Al utilizar este sitio web usted acepta el uso de cookies.  <a href="#">Más información</a>
            </div>
        </div>
    
    </div>
  </body>

<!-- Compressed JavaScript -->
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/what-input.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/foundation.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/howler/2.0.13/howler.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/tinyPlayer.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/app.js"></script>
<script>
   $(document).foundation();
   $(document).ready(function() {

      // carrusel de programas
    $('.owl-carousel-programs').owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      autoplay: true,
      smartSpeed: 500,
      autoplayTimeout: 8000,
      nav: true,
      responsive: {
        0: {
          items: 2,
          nav: true
        },
        768: {
          items: 3,
        },
        1000: {
          items: 4,
          margin: 16,
          autoplay: true
        }
      }
    });
  
  // carrusel de series radiofónicas
    $('.owl-carousel-series').owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      autoplay: true,
      smartSpeed: 500,
      autoplayTimeout: 8000,
      nav: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
       768: {
          items: 2,
        },
        1000: {
          items: 2,
          margin: 16,
          autoplay: true
        }
      }
    });
     
    // carrusel de eyecatcher
    $('.owl-carousel-eye').owlCarousel({
    
    autoplay: true,
    smartSpeed: 500,
    autoplayTimeout: 10000,
    items:1,
    loop: true,
    margin:0,
    autoHeight:true,
    nav: true
    });
    
  });
</script>

</html>